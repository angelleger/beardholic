</]<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:8;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2019-05-06 04:57:28";s:13:"post_date_gmt";s:19:"2019-05-06 04:57:28";s:12:"post_content";s:9971:"<!-- wp:heading -->
<h2>Who we are</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>It is Beard Holic's policy to respect your privacy regarding any information we may collect while operating our website. This Privacy Policy applies to <a href="http://www.beardholic.com/">beardholic.com</a> (hereinafter, "us", "we", or "beardholic.com"). We respect your privacy and are committed to protecting personally identifiable information you may provide us through the Website. We have adopted this privacy policy ("Privacy Policy") to explain what information may be collected on our Website, how we use this information, and under what circumstances we may disclose the information to third parties. This Privacy Policy applies only to information we collect through the Website and does not apply to our collection of information from other sources.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>This Privacy Policy, together with the Terms and conditions posted on our Website, set forth the general rules and policies governing your use of our Website. Depending on your activities when visiting our Website, you may be required to agree to additional terms and conditions.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>What personal data we collect and why we collect it</h2>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>Comments</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>Media</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>Contact forms</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>Cookies</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select "Remember Me", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3>Embedded content from other websites</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Website Visitors</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Like most website operators, Beard Holic collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Beard Holic's purpose in collecting non-personally identifying information is to better understand how Beard Holic's visitors use its website. From time to time, Beard Holic may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Beard Holic also collects potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users and for users leaving comments on http://www.beardholic.com blog posts. Beard Holic only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Gathering of Personally-Identifying Information</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Certain visitors to Beard Holic's websites choose to interact with Beard Holic in ways that require Beard Holic to gather personally-identifying information. The amount and type of information that Beard Holic gathers depend on the nature of the interaction. For example, we ask visitors who sign up for a blog at http://www.beardholic.com to provide a username and email address.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Security</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Advertisements</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Ads appearing on our website may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Beard Holic and does not cover the use of cookies by any advertisers.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Aggregated Statistics</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Beard Holic may collect statistics about the behavior of visitors to its website. Beard Holic may display this information publicly or provide it to others. However, Beard Holic does not disclose your personally-identifying information.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Who we share your data with</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>We don't share your data with nobody.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>How long we retain your data</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>What rights you have over your data</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Where we send your data</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Visitor comments may be checked through an automated spam detection service.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Links To External Sites</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Our Service may contain links to external sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy and terms and conditions of every site you visit.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites, products or services.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2>Contact Information</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>If you have any questions about this Privacy Policy, please contact us via&nbsp;<a href="mailto:contact@beardholic.com">email</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:14:"Privacy Policy";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:14:"privacy-policy";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2019-05-19 01:46:48";s:17:"post_modified_gmt";s:19:"2019-05-19 01:46:48";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:36:"http://www.beardholic.com/?page_id=8";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}