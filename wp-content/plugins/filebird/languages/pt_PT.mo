��    .      �  =   �      �     �             	        (     6     =     D     L     X     ^     g     n     �     �     �     �     �     �  
   �  
   �  
   �     �                         %     ,  +   :     f  7   �  5   �     �  :        L     l     z  {   �          $  "   *     M     c     h  �  n     0
     E
     a
     u
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                %     ,     2  
   B     M     ^  -   k     �     �  	   �     �     �     �  3   �       9   -  4   g  #   �  >   �  %   �     %     3  �   @     �  	   �  "   �          (     /     )   "   (   !   .                                                                   '                         %             &   
         -             $          ,                *                   +   #   	    Add New Folder Add sub folder All categories All files Are you sure? Cancel Delete Deleted Edit Folder Error FileBird Folder Folder Limit Reached Folder not found Folder not found in trash Folders Go Pro Move Move 1 file New Folder New folder Ninja Team No media files found. No, thanks. Oops Refresh Remove Rename Search Folder Select a folder and upload files (Optional) Something isn't correct here. Sorry! An error occurred while processing your request. This folder already exists. Please type another name. This folder cannot be deleted. This folder contains subfolders, please delete them first! This page will be reloaded now. Uncategorized Yes, delete it! You will not be able to recover this folder! But files in this folder are all safe, they are moved to Uncategorized folder. Your folder has been deleted. files https://media-folder.ninjateam.org https://ninjateam.org item items Project-Id-Version: FileBird
POT-Creation-Date: 2019-06-11 10:14+0700
PO-Revision-Date: 2019-06-11 10:16+0700
Last-Translator: 
Language-Team: 
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: filebird.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Adicionar nova pasta Adicionar pasta secundária Todas as categorias Todos os Ficheiros Tem a certeza? Cancelar Excluir Apagado Editar pasta Erro FileBird Pasta Limite de pasta atingido Pasta não encontrada Pasta não encontrada no lixo Pastas Ir pro Mover Mover 1 arquivo Nova Pasta Nový priečinok Equipe ninja Não foram encontrados ficheiros multimédia. Não, obrigado. Oops Atualizar Remover Renomear Pasta de pesquisa Selecionar uma pasta e carregar arquivos (opcional) Algo não está correto aqui. Desculpa! Ocorreu um erro ao processar sua solicitação. Esta pasta já existe. Por favor, digite outro nome. Esta pasta não pode ser excluída. Esta pasta contém subpastas, por favor, excluí-los primeiro! Esta página será recarregada agora. Sem categoria Sim, apague! Você não será capaz de recuperar esta pasta! Mas os arquivos nesta pasta são todos seguros, eles são movidos para Uncategorized pasta. Sua pasta foi excluída. ficheiros https://media-folder.ninjateam.org https://ninjateam.org artigo itens 